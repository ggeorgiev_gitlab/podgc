package common

import (
	"bytes"
	"io"
	"io/ioutil"
	"net/http"

	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	runtimeserializer "k8s.io/apimachinery/pkg/runtime/serializer"
	k8sClient "k8s.io/client-go/kubernetes"
	k8sRestClient "k8s.io/client-go/rest"
	k8sFakeClient "k8s.io/client-go/rest/fake"
)

func GetTestKubernetesClient(version string, httpClient *http.Client) *k8sClient.Clientset {
	conf := k8sRestClient.Config{
		ContentConfig: k8sRestClient.ContentConfig{
			GroupVersion: &schema.GroupVersion{Version: version},
		},
	}
	kube := k8sClient.NewForConfigOrDie(&conf)
	fakeClient := k8sFakeClient.RESTClient{Client: httpClient}
	kube.RESTClient().(*k8sRestClient.RESTClient).Client = fakeClient.Client
	kube.CoreV1().RESTClient().(*k8sRestClient.RESTClient).Client = fakeClient.Client
	kube.ExtensionsV1beta1().RESTClient().(*k8sRestClient.RESTClient).Client = fakeClient.Client

	return kube
}

// minimal port from k8s.io/kubernetes/pkg/testapi
func TestVersionAndCodec() (version string, codec runtime.Codec) {
	scheme := runtime.NewScheme()

	_ = scheme.AddIgnoredConversionType(&metav1.TypeMeta{}, &metav1.TypeMeta{})
	scheme.AddKnownTypes(
		corev1.SchemeGroupVersion,
		&corev1.Pod{},
		&corev1.PodList{},
	)

	codecs := runtimeserializer.NewCodecFactory(scheme)
	codec = codecs.LegacyCodec(corev1.SchemeGroupVersion)
	version = corev1.SchemeGroupVersion.Version

	return
}

func ObjBody(codec runtime.Codec, obj runtime.Object) io.ReadCloser {
	return ioutil.NopCloser(bytes.NewReader([]byte(runtime.EncodeOrDie(codec, obj))))
}
