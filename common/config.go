package common

import (
	"bufio"
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"

	"gitlab.com/gitlab-org/podgc/internal/utils"
)

const (
	DefaultConfigFile = "config.toml"
	defaultLogFile    = "podgc.log"
	defaultLogFormat  = "json"

	logLevelInfo  = "info"
	logLevelError = "error"

	errReadingConfig  = "couldn't read configuration file"
	errDecodingConfig = "couldn't parse TOML content of the configuration file"

	LimitResources = 600

	Pods     = "pods"
	K8sAPIV1 = "v1"
)

//nolint:lll
type KubernetesConfig struct {
	Host        string               `toml:"host,omitempty" json:"host,omitempty" long:"kubernetes-host" env:"KUBERNETES_HOST" description:"Optional Kubernetes master host URL (auto-discovery attempted if not specified)"`
	CertFile    string               `toml:"cert_file,omitempty" json:"certFile,omitempty" long:"kubernetes-cert-file" env:"KUBERNETES_CERT_FILE" description:"Optional Kubernetes master auth certificate"`
	KeyFile     string               `toml:"key_file,omitempty" json:"keyFile,omitempty" long:"kubernetes-key-file" env:"KUBERNETES_KEY_FILE" description:"Optional Kubernetes master auth private key"`
	CAFile      string               `toml:"ca_file,omitempty" json:"caFile,omitempty" long:"kubernetes-ca-file" env:"KUBERNETES_CA_FILE" description:"Optional Kubernetes master auth ca certificate"`
	BearerToken string               `toml:"bearer_token,omitempty" json:"bearer_token,omitempty" long:"kubernetes-bearer-token" env:"KUBERNETES_BEARER_TOKEN" description:"Optional Kubernetes service account token used to start build pods"`
	Namespaces  []string             `toml:"namespaces,omitempty" json:"namespaces,omitempty" long:"kubernetes-namespaces" env:"KUBERNETES_NAMESPACES" description:"List of namespaces to search for Kubernetes pods"`
	Annotations []string             `toml:"annotations,omitempty" json:"annotations,omitempty" long:"kubernetes-annotations" env:"KUBERNETES_ANNOTATIONS" description:"List of annotations to consider for the ttl setting"`
	Resources   *KubernetesResources `toml:"resources,omitempty" json:"resources,omitempty" description:"Details on the resources to clean"`
}

//nolint:lll
type KubernetesResources struct {
	Kind       string `toml:"kind,omitempty" json:"kind,omitempty" long:"kubernetes-kind" env:"KUBERNETES_KIND" description:"The kind of resources to clean. As for now, the only kind supported is \"Pods\" "`
	APIVersion string `toml:"api_version,omitempty" json:"apiVersion,omitempty" long:"kubernetes-api-version" env:"KUBERNETES_API_VERSION" description:"The version of the Kubernetes API to be used to clean the resource. As for now, the only version supported is \"v1\""`
}

//nolint:lll
type LoggingConfig struct {
	LogLevel  string `toml:"log_level,omitempty" json:"logLevel,omitempty" long:"log-level" env:"PODGC_LOG_LEVEL" description:"Optional Log level for the PodGC application. Default to INFO"`
	LogFile   string `toml:"log_file,omitempty" json:"logFile,omitempty" long:"log-file" env:"PODGC_LOG_FILE" description:"Optional File used to save the log produced by the PodGC application. Default podgc.log in the execution folder"`
	LogFormat string `toml:"log_format,omitempty" json:"logFormat,omitempty" long:"log-format" env:"PODGC_LOG_FORMAT" description:"Optional Format used for logging. Accepted values: text and json. Default to json"`
}

//nolint:lll
type PodGCInfo struct {
	Name  string `toml:"name,omitempty" json:"name,omitempty" short:"name" long:"description" env:"PODGC_NAME" description:"PodGC name"`
	Limit int    `toml:"limit" json:"limit" long:"limit" env:"PODGC_LIMIT" description:"Maximum number of Pods to be deleted per cycle"`
}

//nolint:lll
type PodGCSettings struct {
	CheckInterval int               `toml:"check_interval" json:"checkInterval" description:"Deletion interval (in seconds)"`
	Kubernetes    *KubernetesConfig `toml:"kubernetes,omitempty" json:"kubernetes,omitempty" description:"Configuration of the Kubernetes Cluster to be cleaned"`
	PodGCInfo
}

func (ps PodGCSettings) Validate() error {
	if ps.CheckInterval == 0 {
		return fmt.Errorf(`podgc config validation: no "check_interval" sets in the configuration (in seconds)`)
	}

	if len(ps.Kubernetes.Namespaces) == 0 {
		return fmt.Errorf(`podgc config validation: no "namespace" given in the configuration`)
	}

	if len(ps.Kubernetes.Annotations) == 0 {
		return fmt.Errorf(`podgc config validation: no "annotation" given in the configuration`)
	}

	if ps.Kubernetes.Resources == nil {
		return fmt.Errorf(`podgc config validation: no "ressources" specified in the configuration`)
	}

	return nil
}

//nolint:lll
type PodGCConfig struct {
	PodGC []PodGCSettings `toml:"podgc" json:"podgc" description:"PodGC configuration"`

	LoggingConfig
}

func (c *PodGCConfig) SetLoggingConfig(lc LoggingConfig) {
	if lc.LogFile != "" {
		c.LogFile = lc.LogFile
	}
	if lc.LogFormat != "" {
		c.LogFormat = lc.LogFormat
	}
	if lc.LogLevel != "" {
		c.LogLevel = lc.LogLevel
	}
}

func (c *PodGCConfig) AddPodGCSetting(ps PodGCSettings) {
	if c.PodGC == nil {
		c.PodGC = []PodGCSettings{}
	}

	// Default the check interval to 10 min
	if ps.CheckInterval == 0 {
		ps.CheckInterval = 600
	}

	c.PodGC = append(c.PodGC, ps)
}

func NewPodGCConfig() *PodGCConfig {
	return &PodGCConfig{
		LoggingConfig: LoggingConfig{
			LogLevel:  logLevelInfo,
			LogFile:   defaultLogFile,
			LogFormat: defaultLogFormat,
		},
	}
}

func LoadFromFile(file string) (*PodGCConfig, error) {
	cfg := NewPodGCConfig()

	data, err := ioutil.ReadFile(file)
	if err != nil {
		return cfg, fmt.Errorf("%s %q: %w", errReadingConfig, file, err)
	}

	err = toml.Unmarshal(data, cfg)
	if err != nil {
		return cfg, fmt.Errorf("%s: %w", errDecodingConfig, err)
	}

	return cfg, nil
}

func SaveToFile(configFile string, config *PodGCConfig) error {
	var newConfig bytes.Buffer
	newBuffer := bufio.NewWriter(&newConfig)

	if err := toml.NewEncoder(newBuffer).Encode(config); err != nil {
		return err
	}

	if err := newBuffer.Flush(); err != nil {
		return err
	}

	// create directory to store configuration
	err := os.MkdirAll(filepath.Dir(configFile), 0700)
	if err != nil {
		return err
	}

	// write config file
	if err := ioutil.WriteFile(configFile, newConfig.Bytes(), 0600); err != nil {
		return err
	}

	return nil
}

func GeneratePodGCName() string {
	return fmt.Sprintf("podgc-%s", utils.GenerateString(5))
}
