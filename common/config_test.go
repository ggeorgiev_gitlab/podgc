package common

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestLoadFromFile(t *testing.T) {
	assertLogging := func(t *testing.T, config, expectedCfg *PodGCConfig) {
		assert.Equal(t, expectedCfg.LogFile, config.LogFile)
		assert.Equal(t, expectedCfg.LogLevel, config.LogLevel)
		assert.Equal(t, expectedCfg.LogFormat, config.LogFormat)
	}

	assertKubernetes := func(t *testing.T, config, expectedCfg *KubernetesConfig) {
		assert.Equal(t, expectedCfg.Host, config.Host)
		assert.Equal(t, expectedCfg.CertFile, config.CertFile)
		assert.Equal(t, expectedCfg.KeyFile, config.KeyFile)
		assert.Equal(t, expectedCfg.CAFile, config.CAFile)

		assert.Equal(t, len(expectedCfg.Namespaces), len(config.Namespaces))
		assert.Equal(t, expectedCfg.Namespaces, config.Namespaces)

		assert.Equal(t, len(expectedCfg.Annotations), len(config.Annotations))
		assert.Equal(t, expectedCfg.Annotations, config.Annotations)
	}

	tests := map[string]struct {
		config         string
		getExpectedCfg func() *PodGCConfig
		expectErr      bool
		validateConfig func(t *testing.T, config, expectedCfg *PodGCConfig)
		validateError  func(t *testing.T, err error)
	}{
		"Parse successfully empty config": {
			config: ``,
			getExpectedCfg: func() *PodGCConfig {
				return NewPodGCConfig()
			},
			expectErr: false,
		},
		"Parse successfully logging config": {
			config: `
			log_level = "info"
			log_file = "my_log_file.log"
			log_format = "text"
			`,
			getExpectedCfg: func() *PodGCConfig {
				cfg := NewPodGCConfig()
				cfg.LogLevel = "info"
				cfg.LogFile = "my_log_file.log"
				cfg.LogFormat = "text"

				return cfg
			},
			validateConfig: func(t *testing.T, config, expectedCfg *PodGCConfig) {
				assertLogging(t, config, expectedCfg)
			},
			expectErr: false,
		},
		"Error parsing logging config": {
			config: `
			log_level = "info"
			log_file = 2
			log_format = "text"
			`,
			getExpectedCfg: func() *PodGCConfig {
				return NewPodGCConfig()
			},
			expectErr: true,
			validateError: func(t *testing.T, err error) {
				assert.Contains(t, err.Error(), errDecodingConfig)
			},
		},
		"Parse successfully filtration settings": {
			config: `
			[[podgc]]
				name = "my_podgc"
				[podgc.kubernetes]
					namespaces = ["default", "custom_namespace"]
					annotations = ["podgc/ttl"]
			`,
			getExpectedCfg: func() *PodGCConfig {
				cfg := NewPodGCConfig()
				kubernetes := &KubernetesConfig{
					Namespaces:  []string{"default", "custom_namespace"},
					Annotations: []string{"podgc/ttl"},
				}

				cfg.PodGC = []PodGCSettings{
					{
						PodGCInfo: PodGCInfo{
							Name: "my_podgc",
						},
						Kubernetes: kubernetes,
					},
				}

				return cfg
			},
			validateConfig: func(t *testing.T, config, expectedCfg *PodGCConfig) {
				assertLogging(t, config, expectedCfg)

				assert.Equal(t, len(expectedCfg.PodGC), len(config.PodGC))
				require.Len(t, config.PodGC, 1)

				assert.Equal(t, expectedCfg.PodGC[0].Name, config.PodGC[0].Name)
				assert.Equal(t, expectedCfg.PodGC[0].Limit, config.PodGC[0].Limit)

				assertKubernetes(t, config.PodGC[0].Kubernetes, expectedCfg.PodGC[0].Kubernetes)
			},
			expectErr: false,
		},
		"Error parsing filtration settings": {
			config: `
			[[podgc]]
				name = "my_podgc"
				[podgc.kubernetes]
					namespaces = "default"
					annotations = ["podgc/ttl"]
			`,
			getExpectedCfg: func() *PodGCConfig {
				cfg := NewPodGCConfig()

				cfg.PodGC = []PodGCSettings{
					{
						PodGCInfo: PodGCInfo{
							Name: "my_podgc",
						},
						Kubernetes: &KubernetesConfig{},
					},
				}

				return cfg
			},
			expectErr: true,
			validateError: func(t *testing.T, err error) {
				assert.Contains(t, err.Error(), errDecodingConfig)
			},
		},
		"Parse successfully k8s cluster settings": {
			config: `
			[[podgc]]
				name = "my_podgc"
				[podgc.kubernetes]
					host = "localhost"
					cert_file = "my_cert_file"
					key_file = "my_key_file"
					ca_file = "my_ca_file"
			`,
			getExpectedCfg: func() *PodGCConfig {
				cfg := NewPodGCConfig()
				kubernetes := &KubernetesConfig{
					Host:     "localhost",
					CertFile: "my_cert_file",
					KeyFile:  "my_key_file",
					CAFile:   "my_ca_file",
				}

				cfg.PodGC = []PodGCSettings{
					{
						PodGCInfo: PodGCInfo{
							Name: "my_podgc",
						},
						Kubernetes: kubernetes,
					},
				}

				return cfg
			},
			validateConfig: func(t *testing.T, config, expectedCfg *PodGCConfig) {
				assertLogging(t, config, expectedCfg)

				assert.Equal(t, len(expectedCfg.PodGC), len(config.PodGC))
				require.Len(t, config.PodGC, 1)

				assert.Equal(t, expectedCfg.PodGC[0].Name, config.PodGC[0].Name)
				assert.Equal(t, expectedCfg.PodGC[0].Limit, config.PodGC[0].Limit)

				assertKubernetes(t, expectedCfg.PodGC[0].Kubernetes, config.PodGC[0].Kubernetes)
			},
			expectErr: false,
		},
		"Error parsing k8s cluster settings": {
			config: `
			[[podgc]]
				name = "my_podgc"
				[podgc.kubernetes]
					host = ["localhost"]
					cert_file = "my_cert_file"
					key_file = "my_key_file"
					ca_file = "my_ca_file"
			`,
			getExpectedCfg: func() *PodGCConfig {
				cfg := NewPodGCConfig()
				kubernetes := &KubernetesConfig{
					KeyFile: "my_key_file",
					CAFile:  "my_ca_file",
				}

				cfg.PodGC = []PodGCSettings{
					{
						PodGCInfo: PodGCInfo{
							Name: "my_podgc",
						},
						Kubernetes: kubernetes,
					},
				}

				return cfg
			},
			expectErr: true,
			validateError: func(t *testing.T, err error) {
				assert.Contains(t, err.Error(), errDecodingConfig)
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			dir, err := ioutil.TempDir("", "")
			require.NoError(t, err)
			defer os.RemoveAll(dir)

			cfgFile := filepath.Join(dir, "config.toml")

			err = ioutil.WriteFile(cfgFile, []byte(tt.config), 0666)
			require.NoError(t, err)

			cfg, err := LoadFromFile(cfgFile)
			if tt.expectErr {
				assert.Error(t, err)
				assert.NotNil(t, cfg)
			}

			expectedCfg := tt.getExpectedCfg()

			if tt.validateConfig != nil {
				tt.validateConfig(t, cfg, expectedCfg)
			}

			if tt.validateError != nil {
				tt.validateError(t, err)
			}
		})
	}
}
