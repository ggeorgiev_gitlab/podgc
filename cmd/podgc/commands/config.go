package commands

import (
	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/cli"
)

type configCommand struct {
	ConfigFile string `long:"config-file" description:"Optional File used to save the generated configuration file. Default config.toml in the execution folder"`

	common.LoggingConfig
	common.PodGCInfo
	common.KubernetesConfig
	common.KubernetesResources
}

func (c *configCommand) Execute(ctx *cli.Context) error {
	configFile := common.DefaultConfigFile

	if c.ConfigFile != "" {
		configFile = c.ConfigFile
	}

	cfg, err := common.LoadFromFile(configFile)
	if err != nil {
		cfg = common.NewPodGCConfig()
	}

	if c.PodGCInfo.Name == "" {
		c.PodGCInfo.Name = common.GeneratePodGCName()
	}

	c.KubernetesConfig.Resources = &c.KubernetesResources

	setting := common.PodGCSettings{
		PodGCInfo:  c.PodGCInfo,
		Kubernetes: &c.KubernetesConfig,
	}

	err = setting.Validate()
	if err != nil {
		return err
	}

	cfg.SetLoggingConfig(c.LoggingConfig)
	cfg.AddPodGCSetting(setting)

	return common.SaveToFile(configFile, cfg)
}

func NewConfigCommand() cli.Command {
	cmd := new(configCommand)

	return cli.Command{
		Handler: cmd,
		Config: cli.Config{
			Name:        "config",
			Aliases:     []string{"c"},
			Usage:       "Allow the generation of configuration file through cli",
			Description: `The command offers the ability to generate a configuration file through the CLI. You can find more information about it at`,
		},
	}
}
