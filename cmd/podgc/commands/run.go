package commands

import (
	"fmt"

	"gitlab.com/gitlab-org/podgc/cleaners"
	"gitlab.com/gitlab-org/podgc/cleaners/kubernetes"
	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/cli"
	"gitlab.com/gitlab-org/podgc/internal/logging/storage"
)

var (
	closeLogFile = cli.NewNopHook()
)

type runCommand struct {
	ConfigFile string `toml:"config_file,omitempty" json:"ConfigFile,omitempty" long:"config-file" description:"Optional File used to save the generated configuration file. Default config.toml in the execution folder"`
	PodGCName  string `toml:"podgc_name,omitempty" json:"PodGCName,omitempty" long:"podgc-name" description:"Optional Name of the PodGC to start. As the application only launch once instance of podgc, when not specified, the first setting found is used"`

	podGCConfig *common.PodGCConfig
}

func (c *runCommand) Execute(ctx *cli.Context) error {
	err := c.loadConfigurationFile(ctx)
	if err != nil {
		return fmt.Errorf("run command execute: couldn't load the configuration file: %w", err)
	}

	err = c.updateLogLevel(ctx)
	if err != nil {
		return fmt.Errorf("run command execute: couldn't upload the log level: %w", err)
	}

	err = c.updateLogFormat(ctx)
	if err != nil {
		return fmt.Errorf("run command execute: couldn't upload the log format: %w", err)
	}

	err = c.setLoggingToFile(ctx)
	if err != nil {
		return fmt.Errorf("run command execute: couldn't set the logging file: %w", err)
	}

	cleaner, err := c.prepareCleaner(ctx)
	if err != nil {
		return fmt.Errorf("run command execute: couldn't prepare the cleaner: %w", err)
	}

	return c.run(cleaner)
}

func (c *runCommand) run(cl cleaners.Cleaner) error {
	err := cl.Clean()

	return err
}

func (c *runCommand) prepareCleaner(ctx *cli.Context) (cleaners.Cleaner, error) {
	sttgs := c.getCurrentSettings()
	if sttgs == nil {
		return nil, fmt.Errorf("run command init: couldn't retrieve the podgc settings")
	}

	k8sCleaner, err := kubernetes.New(ctx, *sttgs)
	if err != nil {
		return nil, fmt.Errorf("run command init: couldn't initialize the cleaner")
	}

	return k8sCleaner, nil
}

func (c *runCommand) getCurrentSettings() *common.PodGCSettings {
	if c.podGCConfig == nil {
		return nil
	}

	if c.PodGCName == "" {
		return &c.podGCConfig.PodGC[0]
	}

	for _, podgcSetting := range c.podGCConfig.PodGC {
		if podgcSetting.Name == c.PodGCName {
			return &podgcSetting
		}
	}

	return nil
}

func (c *runCommand) loadConfigurationFile(ctx *cli.Context) error {
	configFile := common.DefaultConfigFile

	if c.ConfigFile != "" {
		configFile = c.ConfigFile
	}

	cfg, err := common.LoadFromFile(configFile)
	if err != nil {
		return err
	}

	c.podGCConfig = cfg
	ctx.SetConfig(cfg)

	return nil
}

func (c *runCommand) updateLogLevel(ctx *cli.Context) error {
	logLevel := ctx.Config().LogLevel
	if ctx.Debug() || logLevel == "" {
		return nil
	}

	return ctx.Logger().SetLevel(logLevel)
}

func (c *runCommand) updateLogFormat(ctx *cli.Context) error {
	logFormat := ctx.Config().LogFormat
	if logFormat == "" {
		return nil
	}

	return ctx.Logger().SetFormat(logFormat)
}

func (c *runCommand) setLoggingToFile(ctx *cli.Context) error {
	logFile := ctx.Config().LogFile
	if logFile == "" {
		return nil
	}

	logStorage := storage.NewFile(logFile)
	err := logStorage.Open()
	if err != nil {
		return err
	}

	closeLogFile = func(ctx *cli.Context) error {
		return logStorage.Close()
	}

	ctx.Logger().SetOutput(logStorage)

	return nil
}

func NewRunCommand() cli.Command {
	cmd := new(runCommand)

	return cli.Command{
		Handler: cmd,
		Config: cli.Config{
			Name:        "run",
			Aliases:     []string{"r"},
			Usage:       "Allow the execution of a cleaner",
			Description: `The command offers the ability to execute a specific cleaner. You can find more information about it at`,
		},
	}
}
