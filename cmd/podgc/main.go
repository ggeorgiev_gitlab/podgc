package main

import (
	"context"
	"os"

	podgc "gitlab.com/gitlab-org/podgc"
	"gitlab.com/gitlab-org/podgc/cmd/podgc/commands"
	"gitlab.com/gitlab-org/podgc/internal/cli"
	"gitlab.com/gitlab-org/podgc/internal/logging"
	"gitlab.com/gitlab-org/podgc/internal/signal"
)

type globalFlags struct {
	Debug bool `long:"debug" description:"Set debug log level"`
}

var (
	global = &globalFlags{
		Debug: false,
	}
)

func main() {
	logger := logging.New()

	ctx := startSignalHandler(logger)

	a := setUpApplication(ctx, logger)

	err := a.Run(os.Args)
	if err != nil {
		logger.
			WithError(err).
			Error("Application execution failed")
	}
}

func startSignalHandler(logger logging.Logger) context.Context {
	terminationHandler := signal.NewTerminationHandler(logger)
	go terminationHandler.HandleSignals()

	return terminationHandler.Context()
}

func setUpApplication(ctx context.Context, logger logging.Logger) *cli.App {
	a := cli.New(ctx, podgc.NAME, "Kubernetes Pod Garbage Collector for GitLab Runner")

	a.AddBeforeFunc(func(ctx *cli.Context) error {
		ctx.SetLogger(logger)

		return nil
	})

	a.AddBeforeFunc(updateDebugMode)

	a.AddGlobalFlagsFromStruct(global)

	a.RegisterCommand(commands.NewConfigCommand())
	a.RegisterCommand(commands.NewRunCommand())

	return a
}

func updateDebugMode(ctx *cli.Context) error {
	if global.Debug {
		ctx.SetDebugMode(global.Debug)
		return ctx.Logger().SetLevel("debug")
	}

	return nil
}
