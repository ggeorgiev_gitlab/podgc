package cleaners

import (
	"context"

	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/cli"
	"gitlab.com/gitlab-org/podgc/internal/logging"
)

type AbstractCleaner struct {
	context *cli.Context

	setting common.PodGCSettings
}

func New(ctx *cli.Context, settings common.PodGCSettings) *AbstractCleaner {
	return &AbstractCleaner{
		context: ctx,
		setting: settings,
	}
}

func (cl *AbstractCleaner) Context() context.Context {
	return cl.context.Ctx
}

func (cl *AbstractCleaner) Logger() logging.Logger {
	return cl.context.Logger()
}

func (cl *AbstractCleaner) PodGCName() string {
	return cl.setting.Name
}

func (cl *AbstractCleaner) Namespaces() []string {
	if cl.setting.Kubernetes == nil {
		return nil
	}
	return cl.setting.Kubernetes.Namespaces
}

func (cl *AbstractCleaner) Annotations() []string {
	if cl.setting.Kubernetes == nil {
		return nil
	}
	return cl.setting.Kubernetes.Annotations
}

func (cl *AbstractCleaner) Resources() *common.KubernetesResources {
	if cl.setting.Kubernetes == nil {
		return nil
	}
	return cl.setting.Kubernetes.Resources
}

func (cl *AbstractCleaner) CheckInterval() int {
	return cl.setting.CheckInterval
}

type Cleaner interface {
	Clean() error
}
