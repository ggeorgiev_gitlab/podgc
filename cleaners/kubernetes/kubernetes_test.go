package kubernetes

import (
	"bytes"
	"context"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/podgc/cleaners"
	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/cli"
)

const (
	namespace = "default"
	podgcTTL  = "podgc/ttl"
)

func TestKubernetesNew(t *testing.T) {
	cliContext := cli.NewContext(context.Background())
	verifyFunc := func(t *testing.T, expected, actual *kubernetes) {
		assert.NotNil(t, actual)
		assert.Equal(t, expected.logger, actual.logger)
		assert.Equal(t, expected.AbstractCleaner.Context(), actual.AbstractCleaner.Context())
		assert.Equal(t, expected.AbstractCleaner.CheckInterval(), actual.AbstractCleaner.CheckInterval())
		assert.Equal(t, expected.AbstractCleaner.Resources(), actual.AbstractCleaner.Resources())
	}

	tests := map[string]struct {
		getCLICtx             func() *cli.Context
		getPodGCSettings      func() common.PodGCSettings
		getExpectedK8sCleaner func() *kubernetes
		expectedErr           bool
		verify                func(*testing.T, *kubernetes, *kubernetes)
	}{
		"fail to create k8sCleaner because of invalid PodGCSettings": {
			getCLICtx: func() *cli.Context {
				return &cli.Context{}
			},
			getPodGCSettings: func() common.PodGCSettings {
				return common.PodGCSettings{}
			},
			expectedErr: true,
		},
		"fail to create k8sCleaner because of missing CA File": {
			getCLICtx: func() *cli.Context {
				return cliContext
			},
			getPodGCSettings: func() common.PodGCSettings {
				return common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 10,
					Kubernetes: &common.KubernetesConfig{
						Host:       "custom.host",
						CertFile:   "path/to/cert/file",
						Namespaces: []string{namespace},
					},
				}
			},
			expectedErr: true,
		},
		"fail to create k8sCleaner with certificate base auth because of invalid CA File": {
			getCLICtx: func() *cli.Context {
				return cliContext
			},
			getPodGCSettings: func() common.PodGCSettings {
				return common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 10,
					Kubernetes: &common.KubernetesConfig{
						Host:       "custom.host",
						CertFile:   "path/to/cert/file",
						CAFile:     "path/to/ca/file",
						KeyFile:    "path/to/key/file",
						Namespaces: []string{namespace},
					},
				}
			},
			expectedErr: true,
		},
		"create k8sCleaner with custom host": {
			getCLICtx: func() *cli.Context {
				return cliContext
			},
			getPodGCSettings: func() common.PodGCSettings {
				return common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 10,
					Kubernetes: &common.KubernetesConfig{
						Host:       "custom.host",
						Namespaces: []string{namespace},
					},
				}
			},
			getExpectedK8sCleaner: func() *kubernetes {
				settings := common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 10,
					Kubernetes: &common.KubernetesConfig{
						Host:       "custom.host",
						Namespaces: []string{namespace},
					},
				}
				abstract := cleaners.New(cliContext, settings)
				return &kubernetes{
					logger:          cliContext.Logger().WithField("podgc", abstract.PodGCName()),
					AbstractCleaner: abstract,
				}
			},
			verify: verifyFunc,
		},
		"create k8sCleaner by guessing kubeconfig": {
			getCLICtx: func() *cli.Context {
				return cliContext
			},
			getPodGCSettings: func() common.PodGCSettings {
				return common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 10,
					Kubernetes:    &common.KubernetesConfig{},
				}
			},
			getExpectedK8sCleaner: func() *kubernetes {
				settings := common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 10,
					Kubernetes:    &common.KubernetesConfig{},
				}
				abstract := cleaners.New(cliContext, settings)
				return &kubernetes{
					logger:          cliContext.Logger().WithField("podgc", abstract.PodGCName()),
					AbstractCleaner: abstract,
				}
			},
			verify: verifyFunc,
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			cliCtx := tc.getCLICtx()
			assert.NotNil(t, cliCtx)

			podgcSettings := tc.getPodGCSettings()

			k8sCleaner, err := New(cliCtx, podgcSettings)

			if tc.verify != nil {
				tc.verify(t, tc.getExpectedK8sCleaner(), k8sCleaner)
			}

			if tc.expectedErr {
				assert.Nil(t, k8sCleaner)
				assert.Error(t, err)
			}
		})
	}
}

func TestClean(t *testing.T) {
	tests := map[string]struct {
		getCLICtx             func(context.Context) *cli.Context
		getPodGCSettings      func() common.PodGCSettings
		getExpectedK8sCleaner func() *kubernetes
		expectedErr           bool
		verify                func(*testing.T, string)
	}{
		"clean routine successfully execute": {
			getCLICtx: func(ctx context.Context) *cli.Context {
				return cli.NewContext(ctx)
			},
			getPodGCSettings: func() common.PodGCSettings {
				return common.PodGCSettings{
					PodGCInfo: common.PodGCInfo{
						Name:  "podgc-custom",
						Limit: 5,
					},
					CheckInterval: 1,
					Kubernetes: &common.KubernetesConfig{
						Namespaces:  []string{namespace},
						Annotations: []string{podgcTTL},
						Resources: &common.KubernetesResources{
							Kind:       common.Pods,
							APIVersion: common.K8sAPIV1,
						},
					},
				}
			},
			verify: func(t *testing.T, out string) {
				assert.Contains(t, out, "Starting the cleaning process on a k8s cluster")
				assert.Contains(t, out, "Clean: deletion process triggered ...")
				assert.Contains(t, out, "Clean: deletion process ended ...")
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer

			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()

			cliCtx := tc.getCLICtx(ctx)
			assert.NotNil(t, cliCtx)

			podgcSettings := tc.getPodGCSettings()

			k8sCleaner, err := New(cliCtx, podgcSettings)
			assert.NotNil(t, k8sCleaner)
			assert.Nil(t, err)

			k8sCleaner.logger.SetOutput(&buf)
			k8sCleaner.logger.SetLevel("debug")

			err = k8sCleaner.Clean()

			if tc.verify != nil {
				tc.verify(t, buf.String())
			}

			if tc.expectedErr {
				assert.Nil(t, k8sCleaner)
				assert.Error(t, err)
			}
		})
	}
}
