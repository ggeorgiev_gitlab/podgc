package kubernetes

import (
	"fmt"
	"net/http"
	"time"

	"golang.org/x/sync/errgroup"
	k8sClient "k8s.io/client-go/kubernetes"
	k8sRestClient "k8s.io/client-go/rest"
	k8sCmdClient "k8s.io/client-go/tools/clientcmd"

	"gitlab.com/gitlab-org/podgc/cleaners"
	"gitlab.com/gitlab-org/podgc/cleaners/kubernetes/resources"
	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/cli"
	"gitlab.com/gitlab-org/podgc/internal/logging"
)

var (
	errInvalidConfig = fmt.Errorf("getKubeClientConfig: invalid kubernetes config given")
)

type kubernetes struct {
	*cleaners.AbstractCleaner

	kubeClient *k8sClient.Clientset
	logger     logging.Logger
}

func New(ctx *cli.Context, settings common.PodGCSettings) (*kubernetes, error) {
	kubeConfig, err := getKubeClientConfig(ctx, settings.Kubernetes)
	if err != nil {
		return nil, fmt.Errorf("getting Kubernetes config: %w", err)
	}

	kubeClient, err := k8sClient.NewForConfig(kubeConfig)
	if err != nil {
		return nil, fmt.Errorf("connecting to Kubernetes: %w", err)
	}

	abstract := cleaners.New(ctx, settings)

	return &kubernetes{
		AbstractCleaner: abstract,
		kubeClient:      kubeClient,
		logger:          abstract.Logger().WithField("podgc", abstract.PodGCName()),
	}, nil
}

func getKubeClientConfig(ctx *cli.Context, config *common.KubernetesConfig) (kubeConfig *k8sRestClient.Config, err error) {
	if config == nil {
		return nil, errInvalidConfig
	}

	if len(config.Host) > 0 {
		kubeConfig, err = getOutClusterClientConfig(config)
	} else {
		kubeConfig, err = guessClientConfig()
	}
	if err != nil {
		return nil, fmt.Errorf("getKubeClientConfig: %w", err)
	}

	kubeConfig.UserAgent = ctx.UserAgent()

	return kubeConfig, nil
}

func getOutClusterClientConfig(config *common.KubernetesConfig) (*k8sRestClient.Config, error) {
	kubeConfig := &k8sRestClient.Config{
		Host:        config.Host,
		BearerToken: config.BearerToken,
		TLSClientConfig: k8sRestClient.TLSClientConfig{
			CAFile: config.CAFile,
		},
	}

	// certificate based auth
	if config.CertFile != "" {
		if config.KeyFile == "" || config.CAFile == "" {
			return nil, fmt.Errorf("ca file, cert file and key file must be specified when using file based auth")
		}

		kubeConfig.TLSClientConfig.CertFile = config.CertFile
		kubeConfig.TLSClientConfig.KeyFile = config.KeyFile
	}

	return kubeConfig, nil
}

func guessClientConfig() (*k8sRestClient.Config, error) {
	// Try in cluster config first
	if inClusterCfg, err := k8sRestClient.InClusterConfig(); err == nil {
		return inClusterCfg, nil
	}

	// in cluster config failed. Reading default kubectl config
	return loadDefaultKubectlConfig()
}

func loadDefaultKubectlConfig() (*k8sRestClient.Config, error) {
	config, err := k8sCmdClient.NewDefaultClientConfigLoadingRules().Load()
	if err != nil {
		return nil, fmt.Errorf("loadDefaultKubectlConfig: %w", err)
	}

	return k8sCmdClient.NewDefaultClientConfig(*config, &k8sCmdClient.ConfigOverrides{}).ClientConfig()
}

func (cl *kubernetes) Clean() error {
	errChan := make(chan error)
	cl.logger.Info("Starting the cleaning process on a k8s cluster")

	defer close(errChan)
	defer closeKubeClient(cl.kubeClient)

	go cl.startCleaning(errChan)

	for err := range errChan {
		if err != nil {
			return err
		}
	}

	return nil
}

func (cl *kubernetes) startCleaning(errCh chan<- error) {
	g, ctx := errgroup.WithContext(cl.Context())

	for _, namespace := range cl.Namespaces() {
		resourceManager := resources.New(
			cl.kubeClient, cl.Resources().APIVersion,
			cl.Resources().Kind,
			namespace,
			cl.logger,
		)
		g.Go(func() error {
			return resourceManager.Clean(ctx, time.Duration(cl.CheckInterval())*time.Second, cl.Annotations())
		})
	}

	errCh <- g.Wait()
}

func closeKubeClient(client *k8sClient.Clientset) bool {
	if client == nil {
		return false
	}
	rest, ok := client.CoreV1().RESTClient().(*k8sRestClient.RESTClient)
	if !ok || rest.Client == nil || rest.Client.Transport == nil {
		return false
	}
	if transport, ok := rest.Client.Transport.(*http.Transport); ok {
		transport.CloseIdleConnections()
		return true
	}
	return false
}
