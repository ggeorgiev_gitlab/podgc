package resources

import (
	"context"
	"time"

	k8sClient "k8s.io/client-go/kubernetes"

	v1 "gitlab.com/gitlab-org/podgc/cleaners/kubernetes/resources/v1"
	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/logging"
)

type ResourcesManager interface {
	Clean(context.Context, time.Duration, []string) error
}

func New(kubeclient *k8sClient.Clientset, APIversion, kind, namespace string, logger logging.Logger) ResourcesManager {
	switch APIversion {
	case common.K8sAPIV1:
		switch kind {
		case common.Pods:
			return v1.NewPodsManager(kubeclient, namespace, logger)
		}
	}

	return nil
}
