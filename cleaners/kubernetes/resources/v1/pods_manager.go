package v1

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sClient "k8s.io/client-go/kubernetes"
	typedv1 "k8s.io/client-go/kubernetes/typed/core/v1"

	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/logging"
)

var (
	errPodsRetrieval   = "couldn't retrieve the list of pods"
	errNotValidTTL     = "annotation value read is not valid"
	errCannotDeletePod = "couldn't delete the pod"

	infoNoPods             = "no pods found"
	infoLimitReached       = "limitResources reached"
	infoSuccessfulDeletion = "pod successfully deleted"
)

type PodsManager struct {
	logger logging.Logger

	typedv1.PodInterface
}

func (pm *PodsManager) Clean(ctx context.Context, frequency time.Duration, annotations []string) error {
	ticker := time.NewTicker(frequency)

	for {
		select {
		case <-ctx.Done():
			ticker.Stop()
			return fmt.Errorf(`Clean: %w`, ctx.Err())
		case <-ticker.C:
			pm.logger.Debug("Clean: deletion process triggered ...")

			podlist, err := pm.retrieveAllPods(ctx)
			if err != nil {
				return fmt.Errorf(`Clean: %s: %w`, errPodsRetrieval, err)
			}

			if podlist != nil {
				pm.deletePods(ctx, podlist, annotations)
			}

			pm.logger.Debug("Clean: deletion process ended ...")
		}
	}
}

func (pm *PodsManager) retrieveAllPods(ctx context.Context) (*corev1.PodList, error) {
	pods, err := pm.List(
		ctx,
		metav1.ListOptions{
			TypeMeta: metav1.TypeMeta{
				Kind:       common.Pods,
				APIVersion: common.K8sAPIV1,
			},
		},
	)
	if err != nil {
		return nil, fmt.Errorf("retrieveAllPods: %s %w", errPodsRetrieval, err)
	}

	return pods, nil
}

func (pm *PodsManager) deletePods(ctx context.Context, podList *corev1.PodList, annotations []string) {
	if len(podList.Items) == 0 {
		pm.logger.Info("deletePods: ", infoNoPods)
		return
	}

	count := 0

	for _, pod := range podList.Items {
		if count > common.LimitResources {
			pm.logger.Info("deletePods: ", infoLimitReached)
			return
		}

		log := pm.logger.WithFields(logrus.Fields{
			"name":       pod.Name,
			"created_at": pod.CreationTimestamp,
		})

		for _, annotation := range annotations {
			value, ok := pod.Annotations[annotation]

			if !ok {
				continue
			}

			ttl, err := strconv.Atoi(value)
			if err != nil {
				log.WithField(annotation, value).
					Error(fmt.Errorf("deletePods: %s: %w", errNotValidTTL, err))
				break
			}

			if isExpired(pod.CreationTimestamp, ttl) {
				err = pm.Delete(ctx, pod.Name, metav1.DeleteOptions{})
				if err != nil {
					log.WithField(annotation, value).Error(fmt.Errorf("deletePods: %s: %w", errCannotDeletePod, err))
					break
				}

				log.WithField(annotation, value).Info("deletePods: ", infoSuccessfulDeletion)
				count++
				break
			}
		}
	}
}

func isExpired(creationTime metav1.Time, ttl int) bool {
	return creationTime.Add(time.Duration(ttl) * time.Second).Before(time.Now().UTC())
}

func NewPodsManager(kubeclient *k8sClient.Clientset, namespace string, logger logging.Logger) *PodsManager {
	return &PodsManager{
		PodInterface: kubeclient.CoreV1().Pods(namespace),
		logger:       logger.WithField("namespace", namespace),
	}
}
