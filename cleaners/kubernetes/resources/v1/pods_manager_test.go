package v1

import (
	"bytes"
	"context"
	"fmt"
	"net/http"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	k8sFakeClient "k8s.io/client-go/rest/fake"

	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/logging"
)

const (
	namespace = "default"
	podgcTTL  = "podgc/ttl"
	customTTL = "custom/ttl"
)

var (
	errClientFunc = fmt.Errorf("unexpected request")

	thirtySecAgo = getMetaTime(-30)
	tenSecAgo    = getMetaTime(-10)
)

func TestRetrieveAllPodsV1(t *testing.T) {
	count := 5
	version, codec := common.TestVersionAndCodec()

	podList := generatePodList(count, namespace, nil, 0)
	assert.NotNil(t, podList)

	logger := logging.New()
	logger.SetFormat(logging.FormatJSON)

	tests := map[string]struct {
		clientFunc     func(*http.Request) (*http.Response, error)
		expectedResult *corev1.PodList
		expectedErr    error
		verifyResult   func(*testing.T, *corev1.PodList, *corev1.PodList)
	}{
		"retrieve empty list of pods": {
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					pods := &corev1.PodList{
						TypeMeta: metav1.TypeMeta{
							Kind:       "",
							APIVersion: "",
						},
						Items: nil,
					}
					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       common.ObjBody(codec, pods),
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				default:
					return nil, errClientFunc
				}
			},
			expectedResult: &corev1.PodList{
				TypeMeta: metav1.TypeMeta{
					Kind:       "",
					APIVersion: "",
				},
				Items: nil,
			},
			verifyResult: func(t *testing.T, expected, actual *corev1.PodList) {
				assert.Equal(t, expected.Kind, actual.Kind)
				assert.Equal(t, expected.APIVersion, actual.APIVersion)
				assert.ElementsMatch(t, expected.Items, actual.Items)
			},
		},
		"retrieve not empty list of pods": {
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       common.ObjBody(codec, podList),
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				default:
					// Ensures no GET is performed when deleting by name
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			expectedResult: podList,
			verifyResult: func(t *testing.T, expected, actual *corev1.PodList) {
				assert.Equal(t, expected.Kind, actual.Kind)
				assert.Equal(t, expected.APIVersion, actual.APIVersion)
				assert.ElementsMatch(t, expected.Items, actual.Items)
				assert.Len(t, actual.Items, count)
			},
		},
		"fail to retrieve list of pods": {
			clientFunc: func(req *http.Request) (*http.Response, error) {
				return nil, errClientFunc
			},
			expectedErr: fmt.Errorf("%s %w", errPodsRetrieval, errClientFunc),
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			fake := common.GetTestKubernetesClient(common.K8sAPIV1, k8sFakeClient.CreateHTTPClient(tc.clientFunc))

			pm := NewPodsManager(fake, namespace, logger)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			pods, err := pm.retrieveAllPods(ctx)

			if tc.verifyResult != nil {
				tc.verifyResult(t, tc.expectedResult, pods)
			}

			if tc.expectedErr != nil {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), errPodsRetrieval)
				assert.Contains(t, err.Error(), errClientFunc.Error())
				assert.Nil(t, pods)
			}
		})
	}
}

func TestDeletePods(t *testing.T) {
	logger := logging.New()
	logger.SetFormat(logging.FormatJSON)

	deleteClientFunc := func(req *http.Request) (*http.Response, error) {
		switch m := req.Method; {
		case m == http.MethodDelete:
			return &http.Response{
				StatusCode: http.StatusOK,
				Header:     map[string][]string{"Content-Type": {"application/json"}},
			}, nil
		default:
			return nil, errClientFunc
		}
	}

	tests := map[string]struct {
		getPodList   func() *corev1.PodList
		annotations  []string
		clientFunc   func(*http.Request) (*http.Response, error)
		verifyResult func(*testing.T, string)
	}{
		"delete some pods from the list": {
			getPodList: func() *corev1.PodList {
				podList := generatePodList(10, namespace, nil, 0)
				assert.NotNil(t, podList)
				assert.NotEmpty(t, podList.Items)

				podList.Items[2].Annotations = map[string]string{podgcTTL: "10"}
				podList.Items[2].CreationTimestamp = thirtySecAgo

				podList.Items[3].Annotations = map[string]string{customTTL: "10"}
				podList.Items[3].CreationTimestamp = thirtySecAgo

				podList.Items[5].Annotations = map[string]string{podgcTTL: "30"}
				podList.Items[5].CreationTimestamp = tenSecAgo

				podList.Items[6].Annotations = map[string]string{customTTL: "30"}
				podList.Items[6].CreationTimestamp = tenSecAgo

				return podList
			},
			annotations: []string{podgcTTL},
			clientFunc:  deleteClientFunc,
			verifyResult: func(t *testing.T, out string) {
				assert.Regexp(t, fmt.Sprintf(`(?m)"msg":"deletePods: %s".*"name":"pod-2"`, infoSuccessfulDeletion), out)
			},
		},
		"delete from an empty list of pods": {
			getPodList: func() *corev1.PodList {
				return generatePodList(0, namespace, nil, 0)
			},
			annotations: []string{podgcTTL},
			clientFunc:  deleteClientFunc,
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, infoNoPods)
			},
		},
		"reach the maximum of pods allowed": {
			getPodList: func() *corev1.PodList {
				return generatePodList(12, namespace, map[string]string{podgcTTL: "10"}, 30)
			},
			annotations: []string{podgcTTL},
			clientFunc:  deleteClientFunc,
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, infoLimitReached)
			},
		},
		"cannot delete because of invalid TTL value": {
			getPodList: func() *corev1.PodList {
				podList := generatePodList(10, namespace, nil, 0)
				assert.NotNil(t, podList)
				assert.NotEmpty(t, podList.Items)

				podList.Items[2].Annotations = map[string]string{podgcTTL: "not a number"}
				podList.Items[2].CreationTimestamp = thirtySecAgo

				return podList
			},
			annotations: []string{podgcTTL},
			clientFunc:  deleteClientFunc,
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, errNotValidTTL)
			},
		},
		"cannot delete pod because of kube error": {
			getPodList: func() *corev1.PodList {
				podList := generatePodList(10, namespace, nil, 0)
				assert.NotNil(t, podList)
				assert.NotEmpty(t, podList.Items)

				podList.Items[2].Annotations = map[string]string{podgcTTL: "10"}
				podList.Items[2].CreationTimestamp = thirtySecAgo

				return podList
			},
			annotations: []string{podgcTTL},
			clientFunc: func(req *http.Request) (*http.Response, error) {
				return nil, errClientFunc
			},
			verifyResult: func(t *testing.T, out string) {
				assert.Contains(t, out, errCannotDeletePod)
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			fake := common.GetTestKubernetesClient(common.K8sAPIV1, k8sFakeClient.CreateHTTPClient(tc.clientFunc))

			pm := NewPodsManager(fake, namespace, logger)

			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			podList := tc.getPodList()
			pm.deletePods(ctx, podList, tc.annotations)

			if tc.verifyResult != nil {
				tc.verifyResult(t, buf.String())
			}
		})
	}
}

func TestClean(t *testing.T) {
	version, codec := common.TestVersionAndCodec()

	logger := logging.New()
	logger.SetFormat(logging.FormatJSON)

	annotations := []string{podgcTTL}

	tests := map[string]struct {
		clientFunc   func(*http.Request) (*http.Response, error)
		frequency    int
		execute      func(*PodsManager, int) error
		verifyResult func(*testing.T, string, error)
	}{
		"clean successfully pods": {
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					podList := generatePodList(10, namespace, nil, 0)
					assert.NotNil(t, podList)
					assert.NotEmpty(t, podList.Items)

					podList.Items[2].Annotations = map[string]string{podgcTTL: "10"}
					podList.Items[2].CreationTimestamp = thirtySecAgo

					return &http.Response{
						StatusCode: http.StatusOK,
						Body:       common.ObjBody(codec, podList),
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				default:
					// Ensures no GET / DELETE is performed
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			frequency: 1,
			execute: func(pm *PodsManager, frequency int) error {
				ctx, cancel := context.WithCancel(context.Background())

				go func() {
					time.Sleep(time.Duration(frequency*1000+500) * time.Millisecond)
					cancel()
				}()

				return pm.Clean(ctx, time.Duration(frequency)*time.Second, annotations)
			},
			verifyResult: func(t *testing.T, out string, err error) {
				assert.Regexp(t, fmt.Sprintf(`(?m)"msg":"deletePods: %s".*"name":"pod-2"`, infoSuccessfulDeletion), out)

				assert.Error(t, err)
			},
		},
		"fail to retrieve pods": {
			clientFunc: func(req *http.Request) (*http.Response, error) {
				switch p, m := req.URL.Path, req.Method; {
				case p == "/api/"+version+"/namespaces/"+namespace+"/pods" && m == http.MethodGet:
					return nil, errClientFunc
				case m == http.MethodDelete:
					return &http.Response{
						StatusCode: http.StatusOK,
						Header:     map[string][]string{"Content-Type": {"application/json"}},
					}, nil
				default:
					// Ensures no GET / DELETE is performed
					t.Errorf("unexpected request: %s %#v\n%#v", req.Method, req.URL, req)
					return nil, errClientFunc
				}
			},
			frequency: 1,
			execute: func(pm *PodsManager, frequency int) error {
				ctx, cancel := context.WithCancel(context.Background())
				defer cancel()

				return pm.Clean(ctx, time.Duration(frequency)*time.Second, annotations)
			},
			verifyResult: func(t *testing.T, out string, err error) {
				assert.Error(t, err)
				assert.Contains(t, err.Error(), errPodsRetrieval)
				assert.Contains(t, err.Error(), errClientFunc.Error())
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			var buf bytes.Buffer
			logger.SetOutput(&buf)

			fake := common.GetTestKubernetesClient(common.K8sAPIV1, k8sFakeClient.CreateHTTPClient(tc.clientFunc))

			pm := NewPodsManager(fake, namespace, logger)
			err := tc.execute(pm, tc.frequency)

			if tc.verifyResult != nil {
				tc.verifyResult(t, buf.String(), err)
			}
		})
	}
}

func generatePodList(count int, namespace string, annotations map[string]string, ttl int64) *corev1.PodList {
	pods := &corev1.PodList{
		TypeMeta: metav1.TypeMeta{
			Kind:       "",
			APIVersion: "",
		},
		Items: nil,
	}

	if count == 0 {
		return pods
	}

	pods.Items = make([]corev1.Pod, 0)

	for i := 0; i < count; i++ {
		pod := corev1.Pod{
			TypeMeta: metav1.TypeMeta{
				Kind:       common.Pods,
				APIVersion: common.K8sAPIV1,
			},
			ObjectMeta: metav1.ObjectMeta{
				Name:         fmt.Sprintf("pod-%d", i),
				GenerateName: fmt.Sprintf("pod-%d", i),
				Namespace:    namespace,
			},
		}

		if annotations != nil {
			pod.Annotations = annotations
		}

		if ttl > 0 {
			pod.CreationTimestamp = getMetaTime(ttl)
		}

		pods.Items = append(pods.Items, pod)
	}

	return pods
}

func getMetaTime(ttl int64) metav1.Time {
	t := time.Now().UTC().Add(time.Duration(ttl) * time.Second)
	return metav1.NewTime(t)
}
