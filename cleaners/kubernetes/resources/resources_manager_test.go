package resources

import (
	"testing"

	"github.com/stretchr/testify/assert"
	v1 "gitlab.com/gitlab-org/podgc/cleaners/kubernetes/resources/v1"
	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/logging"

	k8sFakeClient "k8s.io/client-go/rest/fake"
)

const (
	namespace = "default"
)

func TestResourceManagerCreation(t *testing.T) {
	logger := logging.New()
	logger.SetFormat(logging.FormatJSON)

	tests := map[string]struct {
		kind         string
		verifyResult func(*testing.T, ResourcesManager)
	}{
		"create successfully a podManager": {
			kind: common.Pods,
			verifyResult: func(t *testing.T, rm ResourcesManager) {
				assert.NotNil(t, rm)
				assert.IsType(t, &v1.PodsManager{}, rm)
			},
		},
		"fail to create a ressource manager": {
			kind: "unknown",
			verifyResult: func(t *testing.T, rm ResourcesManager) {
				assert.Nil(t, rm)
				assert.IsType(t, nil, rm)
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {

			fake := common.GetTestKubernetesClient(common.K8sAPIV1, k8sFakeClient.CreateHTTPClient(nil))

			rm := New(fake, common.K8sAPIV1, tc.kind, namespace, logger)

			if tc.verifyResult != nil {
				tc.verifyResult(t, rm)
			}
		})
	}
}
