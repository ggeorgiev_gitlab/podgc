package cli

import (
	stdContext "context"
	"fmt"

	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/podgc/common"
	"gitlab.com/gitlab-org/podgc/internal/logging"
)

type Context struct {
	Ctx stdContext.Context
	Cli *cli.Context

	debug bool

	config *common.PodGCConfig
	logger logging.Logger
}

func NewContext(ctx stdContext.Context) *Context {
	return &Context{
		Ctx:    ctx,
		logger: logging.New(),
	}
}

func (c *Context) UserAgent() string {
	if c.Cli == nil {
		return "podgc-ua"
	}
	return fmt.Sprintf(
		"%s %s (%s; %s; %s/%s)",
		c.Cli.App.Name, c.Cli.App.Version, c.Cli.App.Metadata[metadataBranch],
		c.Cli.App.Metadata[metadataGOversion], c.Cli.App.Metadata[metadataOS],
		c.Cli.App.Metadata[metadataArchitecture],
	)
}

func (c *Context) SetDebugMode(debug bool) {
	c.debug = debug
}

func (c *Context) Debug() bool {
	return c.debug
}

func (c *Context) SetConfig(cfg *common.PodGCConfig) {
	c.config = cfg
}

func (c *Context) Config() *common.PodGCConfig {
	return c.config
}

func (c *Context) SetLogger(logger logging.Logger) {
	c.logger = logger
}

func (c *Context) Logger() logging.Logger {
	return c.logger
}
