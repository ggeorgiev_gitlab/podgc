# PodGC

- Title: Kubernetes Pod Garbage Collation
- Team: ~"group::runner"
- Issues reference:
  - <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27870>
  - <https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27702>

## Introduction

### Overview

Delete Kubernetes Pods that are no longer used to stop using resources, and only
have Pods that are necessary.

### Goal

- Least privileged should only have list/delete on the `pods` resource.
- Compliant with OpenShift.
- Running rootless container.
- Concurrent safe.
- Easy to delay.
- Audit log of what was deleted and why.
- The user decides which pods are deleted.
- CLI tool that can run inside a Cluster or users laptop.
- Support supported versions of Kubernetes.

### Non-Goals

- Automatically deciding if a pod is expired or not, the user decides this.

## Solution

### Overview

`PodGC` is an application that aims to clean the leftover pods created by executing a job with GitLab Runner Kubernetes executor.

`PodGC` is open-source and written in Go. It can be run as a single binary; no language-specific requirements are needed.
You can install GitLab Runner on several different supported operating systems.

### Execution diagramm

All the Pods that want to get garbage collected need to have `podgc.gitlab.com/ttl` Pod Annotation (configurable).
The value is an integer representing in seconds how long the Pod should be alive for.
For example `podgc.gitlab.com/ttl: 3600` means that the Pod can be deleted after it's been created for 1 hour.

When `podgc` finds a pod that is expired, it sends a [delete Pod](https://kubernetes.io/docs/reference/kubernetes-api/workload-resources/pod-v1/#delete-delete-a-pod) request, when multiple expired pods are found it will send the delete request sequentially for the initial iteration.

```mermaid
sequenceDiagram
    podgc->>+kubernetes: GET /api/v1/pods
    kubernetes->>-podgc: list objects of kind Pod
    Note right of podgc: Calculate expired pods
    podgc->>+kubernetes: DELETE /api/v1/namespaces/{namespace}/pods/{name}
```

### Application configuration with CLI Command

The CLI Command needed is

```sh
podgc config ...
```

or with the debug mode activated

```sh
podgc --debug config ...
```

This command accepts the following parameters:

| Setting                    | Description                                                                                                        |
|----------------------------|--------------------------------------------------------------------------------------------------------------------|
|`--config-file`             | Optional File used to save the generated configuration file. Default config.toml in the execution folder           |
|`--log-level`               | Optional Log level for the PodGC application. Default to `info`                                                    |
|`--log-file`                | Optional File used to save the generated configuration file. Default config.toml in the execution folder           |
|`--log-format`              | Optional Format used for logging. Accepted values: text and json. Default to json                                  |
|`--name`, `--description`   | PodGC name. Auto Generated if not provided                                                                         |
|`--limit`                   | Maximum number of Pods to be deleted per cycle (default: "10")                                                     |
|`--kubernetes-host`         | Optional Kubernetes master host URL (auto-discovery attempted if not specified)                                    |
|`--kubernetes-cert-file`    | Optional Kubernetes master auth certificate                                                                        |
|`--kubernetes-key-file`     | Optional Kubernetes master auth private key                                                                        |
|`--kubernetes-ca-file`      | Optional Kubernetes master auth ca certificate                                                                     |
|`--kubernetes-bearer-token` | Optional Kubernetes service account token used to start build pods                                                 |
|`--kubernetes-namespaces`   | List of the namespaces to search for Kubernetes pods                                                               |
|`--kubernetes-annotations`  | List of the annotations to consider for the ttl setting                                                            |
|`--kubernetes-kind`         | The kind of resources to clean. As for now, the only kind supported is `pods`                                      |
|`--kubernetes-api-version`  | The version of the Kubernetes API to be used to clean the resource. As for now, the only version supported is `v1` |

The same details can be retrieved by running the CLI command:

```sh
podgc config --help
```

### Application configuration with TOML File editing

The configuration can also be created manually through a `TOML` file. Hereafter an example:

```toml
log_level = "info"
log_file = ""
log_format = "text"

[[podgc]]
  name = "podgc-Qdsas"
  check_interval = 10
  [podgc.kubernetes]
    namespaces = ["default", "custom-namespace"]
    annotations = ["podgc/ttl"]
    [podgc.kubernetes.resources]
      kind = "pods"
      api_version = "v1"
```

The parameters needed are the following:

| Setting                    | Type                  | Required | Description                                                                                                        |
|----------------------------|-----------------------|----------|--------------------------------------------------------------------------------------------------------------------|
|`log_level`                 | `string`              | No       | Optional Log level for the PodGC application. Accepted values are `info`, `error`. Default to `info`               |
|`log_file`                  | `string`              | No       | Optional Path to file used to save the generated configuration file. Default config.toml in the execution folder   |
|`log_format`                | `string`              | No       | Optional Format used for logging. Accepted values: `text` and `json`. Default to json                              |
|`podgc`                     | List `PodGCSettings`  | Yes      | Actual settings for the `PodGC` application. More than one setting can be saved in the same configuration files    |

#### PodGCSettings

`PodGCSettings` represents the actual settings for the `PodGC` application to run.

| Setting                    | Type                  | Required | Description                                                                                                        |
|----------------------------|-----------------------|----------|--------------------------------------------------------------------------------------------------------------------|
|`check_interval`            | `int`                 | No       | Deletion interval in seconds. Default to `600 seconds`                                                             |
|`name`                      | `string`              | Yes      | Required PodGC name                                                                                                |
|`limit`                     | `int`                 | No       | Maximum number of Pods to be deleted per cycle. Default `10`                                                       |
|`kubernetes`                | `KubernetesConfig`    | Yes      | Configuration related to the Kubernetes environment where the cleaning will occur                                  |

#### KubernetesConfig

`KubernetesConfig` represents the configuration of the Kubernetes environments where the cleaning will occur.
It is also used to set informations about the `namespaces` to clean, the `annotations` to look for,
the target resources `kind` and the `api version` used.

| Setting                    | Type                 | Required | Description                                                                                                        |
|----------------------------|----------------------|----------|--------------------------------------------------------------------------------------------------------------------|
|`kubernetes_host`           | `string`             | No       | Optional Kubernetes main host URL (auto-discovery attempted if not specified)                                      |
|`kubernetes_cert_file`      | `string`             | No       | Optional Kubernetes main auth certificate                                                                          |
|`kubernetes_key_file`       | `string`             | No       | Optional Kubernetes main auth private key                                                                          |
|`kubernetes_ca_file`        | `string`             | No       | Optional Kubernetes main auth ca certificate                                                                       |
|`kubernetes_bearer_token`   | `string`             | No       | Optional Kubernetes service account token used to start build pods                                                 |
|`kubernetes_namespaces`     | List `string`        | Yes      | Namespaces to search for Kubernetes pods                                                                           |
|`kubernetes_annotations`    | List `string`        | Yes      | Annotations to consider for the ttl setting                                                                        |
|`resources`                 | `KubernetesResources`| Yes      | Annotations to consider for the ttl setting                                                                        |

#### KubernetesResources

`KubernetesResources` allows the settings of the resources details such as the `kind` and the `api version` used.

| Setting                    | Type                 | Required | Description                                                                                                        |
|----------------------------|----------------------|----------|--------------------------------------------------------------------------------------------------------------------|
|`kind`                      | `string`             | No       | The kind of resources to clean. As for now, the only kind supported is `Pods`                                      |
|`api_version`               | `string`             | No       | The version of the Kubernetes API to be used to clean the resource. As for now, the only version supported is `v1` |

### Application execution

Once launched, `PodGC` keeps running until it is stopped through a `SIGTERM` or `SIGKILL` signal. More information can be found in the [Execution Diagramm section](#execution-diagramm)

The application is launched using the following command:

```sh
podgc run ...
```

or with the debug mode activated

```sh
podgc --debug run ...
```

This command accepts the following parameters:

| Setting                    | Description                                                                                                        |
|----------------------------|--------------------------------------------------------------------------------------------------------------------|
|`--config-file`             | Optional Path to the configuration file to used. Default to the config.toml in the execution folder                |
|`--podgc-name`              | Optional Name of the PodGC Settings to used. Default to `the 1st one if the configuration file`                    |

The same details can be retrieved by running the CLI command:

```sh
podgc run --help
```

## What is left to do

As it stands, the following tasks left to be done :

- [x] Unit tests. Partially done. The `cmd/podgc/commands/*.go` files are not covered yet
- [ ] E2E tests
- [x] Full documentation. The current `readme.md` file can be completed if more details are needed
- [ ] Compliant with OpenShift
- [ ] Running rootless container
- [ ] CI

### Goals coverage

- [Least privileged should only have list/delete on the `pods` resource](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/v1/pods_manager.go#L35-58)
- [Audit log of what was deleted and why](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/v1/pods_manager.go#L96-121)
- The user decides which pods are deleted:
  - [Namespaces verification](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/kubernetes.go#L132-142)
  - [Annotations verification](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/v1/pods_manager.go#L97)
- [CLI tool that can run inside a Cluster or users laptop](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cmd/podgc/commands/run.go#L24-51)
- [Support supported versions of Kubernetes](https://gitlab.com/ratchade/podgc/-/blob/fd0548ab2297750ba3f6acc217007445a78237da/cleaners/kubernetes/resources/resources_manager.go#L18-28)
